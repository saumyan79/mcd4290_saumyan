function objectToHTML(obj){
let formattedString= "";
for(let iteration in obj){
    formattedString+= iteration+ ": "+ obj[iteration]+ "\n";
}
return formattedString;
}

const testObj = {
    number: 1,
    string: "abc",
    array: [5, 4, 3, 2, 1],
    boolean: true
   };

   const outputString=objectToHTML(testObj);

   document.getElementById("outputArea1").innerText = outputString;