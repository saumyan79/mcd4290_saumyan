//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    
    let randomNumbers=[54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, 
        -51, -17, -25];

        let positiveOdd=[];
        let negativeEven=[];

        for(i=0; i< randomNumbers.length; i++){
            if(randomNumbers[i]>0 && randomNumbers[i]%2 !==0){
                positiveOdd.push(randomNumbers[i]);
            } else if (randomNumbers[i]<0 && randomNumbers[i]%2===0 ){
                negativeEven.push(randomNumbers[i]);
            }
        }

        output += "Positive Odd: " + positiveOdd + "\n";

        output += "Negative Even: " + negativeEven;
            

     let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here 
    
    let face1=0;
    let face2=0;
    let face3=0;
    let face4=0;
    let face5=0;
    let face6=0;

    for(i=0; i<60000; i++){
        const randomValue= Math.floor((Math.random() * 6) + 1);

        if(randomValue===1){
            face1++;  // face1=face1+1;

        }else if(randomValue===2){
            face2++;
            
        }else if(randomValue===3){
            face3++;

        }else if (randomValue===4){
            face4++;

        }else if(randomValue===5){
            face5++;

        }else if(randomValue===6){
            face6++;
        }
    
    }
     
    
output += "Frequency of die rolls\n"; 

output += "1:" + face1+ "\n";
output += "2:" + face2+ "\n";
output += "3:" + face3+ "\n";
output += "4:" + face4+ "\n";
output += "5:" + face5+ "\n";
output += "6:" + face6+ "\n";



    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here 
    
let frequencyArray=[0, 0, 0, 0, 0, 0, 0];

for(i=0; i<60000; i++){

    const randomValue= Math.floor((Math.random() * 6) + 1);

    frequencyArray[randomValue]++;
}

output+="Frequency of die rolls\n";
output+="1:" + frequencyArray[1] + "\n";
output+="2:" + frequencyArray[2] + "\n";
output+="3:" + frequencyArray[3] + "\n";
output+="4:" + frequencyArray[4] + "\n";
output+="5:" + frequencyArray[5] + "\n";
output+="6:" + frequencyArray[6] + "\n";


    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    
    var dieRolls = {
        Frequencies: {
        1:0,
        2:0,
        3:0,
        4:0,
        5:0,
        6:0,
        },
        Total:60000,
        Exceptions: ""
        }

        for(i=0; i<60000; i++){
    const randomValue= Math.floor((Math.random() * 6) + 1);
            dieRolls.Frequencies[randomValue]++;
        }

        output+= "Frequency of dice rolls"+"\n";
        output+="Total rolls:"+ dieRolls.Total +"\n";
        output+= "Frequencies:"+"\n";

        for(let dieFace in dieRolls.Frequencies){
            output+=dieFace+ ":" + dieRolls.Frequencies[dieFace]+ "\n";
        }
        
        let exceptionalDieFaces="";

        for(let dieFace in dieRolls.Frequencies){
            if(dieRolls.Frequencies[dieFace]<9900  || dieRolls.Frequencies[dieFace]>10100){
                exceptionalDieFaces+= dieFace +",";
            }
        }

        output+="Exceptions: "+ exceptionalDieFaces+ "\n";

    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
    

    let person ={
        name: "Jane",
        income:127050
      };
      
      
      
      
      
      if(person.income>=180001){
        tax=person.income-180000;
        taxOwed=54097+ (tax*45)/100;
      } else if(person.income>90001 && person.income<180000 ){
        tax=person.income-90000;
        taxOwed=20797 + (tax*37)/100;
       }else if(person.income>37001  && person.income<90000){
        tax=person.income-37000;
        taxOwed=3572+ (tax*32.5)/100;
      }else if(person.income>18201  && person.income<37000){
        tax=person.income-18200;
        taxOwed=(tax*19)/100;
      }

      output+= person.name + "'s"+ " income is"+ " $"+ person.income+ ","+ " and her tax owed is:"+ " $"+ taxOwed.toFixed(2);
  
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}